<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "enhancement" label:

- https://gitlab.com/nyhtfury/starforged/-/issues?label_name%5B%5D=enhancement

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary & Rationale

<!-- Summarize the enhancement concisely. What value will this add? -->

### Mockups / Inspiration

<!-- If you have mockups or sources of inspiration for this enhancement, that is helpful. -->

/label "enhancement"
